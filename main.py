def count_table_dimensions(file_path):
    with open(file_path, 'r') as f:
        header = f.readline()
        columns = len(header.strip().split('\t'))

        rows = 0

        for _ in f:
            rows += 1

    return columns, rows + 1


def verify_dimensions(cols, rows):
    print(f"This table has {cols} columns and {rows} rows (including table header)")
    confirm = input("Are these dimensions correct? (y/n)")
    if confirm.lower() != 'y':
        cols = int(input("Enter the number of columns:"))
        rows = int(input("Enter the number of rows:"))
    return cols, rows


def generate_markdown_table(cols, rows):
    table = '```\n'
    table += '|' + ' th |' * cols + '\n'
    table += '|' + ' : --- : |' * cols + '\n'

    for i in range(rows - 1):
        table += '|' + ' td |' * cols + '\n'

    table += '```'
    return table


def main():
    file_path = "table.txt"
    columns, rows = count_table_dimensions(file_path)
    columns, rows = verify_dimensions(columns, rows)
    table = generate_markdown_table(columns, rows)
    print(table)


if __name__ == "__main__":
    main()
